const express=require('express')

const router=express.Router();
const cryptoJS=require('crypto-js')
const mysql=require('mysql2');
const db=require('../db')
const utils=require('../utils');
const { request } = require('http');
const { response } = require('express');
/*id        | int         | NO   | PRI | NULL    | auto_increment |
| title     | varchar(20) | YES  |     | NULL    |                |
| m_release | varchar(20) | YES  |     | NULL    |                |
| m_time    | varchar(10) | YES  |     | NULL    |                |
| director*/


router.get('/',(request,response)=>{
    console.log("hi")
const query='select id,title,m_release,m_time,director from movie';
db.pool.execute(query,(error,movie)=>{
    response.send(utils.createResult(error,movie))
})

router.post('/add',(request,response)=>{
    const {title,m_release,m_time,director}=request.body;
    
    const query='insert into movie (title,m_release,m_time,director) values(?,?,?,?)';

    db.pool.execute(query,[title,m_release,m_time,director],(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

router.get('/get',(request,response)=>{
    const {id}=request.body;
    const query='select title,m_release,m_time,director from movie where id=?';
    db.pool.execute(query,[id],(error,movie)=>{
        if(movie.length >0)
        {
            response.send(utils.createResult(error,movie));
        }
        else{
            response.send("movie not found!!");
        }
        
    })
})

router.delete('/delete',(request,response)=>{
    const {id}=request.body;
    const query='delete from movie where id=?'
    db.pool.execute(query,[id],(error,movie)=>{
        response.send(utils.createResult(error,movie));
    })
})

})
module.exports= router
