const express=require('express')
const cors=require('cors');
const { request } = require('http');
const { response } = require('express');
const app=express();
app.use(cors('*'))
app.use(express.json())

const router=require('./routes/movie')
app.use('/movie',router)
app.listen(3000,'0.0.0.0',()=>{
    console.log('Server started on port number 3000')
})
